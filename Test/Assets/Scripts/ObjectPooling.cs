﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Class, that pooling objects and didn't create new ones if it isn't necessary 
/// </summary>
public class ObjectPooling : MonoBehaviour
{
    public GameObject poolObject;
    private int _amountPooled;

    public List<GameObject> pooledObjects;
    
    // Start is called before the first frame update
    void Start()
    {
        _amountPooled = 20;
        pooledObjects = new List<GameObject>();
        for (int i = 0; i < _amountPooled; i++)
        {
            pooledObjects.Add(Instantiate(poolObject));
            pooledObjects[i].SetActive(false);
            pooledObjects[i].transform.SetParent(this.gameObject.transform);
        }    
    }

    /// <summary>
    /// Function, that pool the object, if free place there is, or create a new object
    /// </summary>
    /// <returns>a value of pooled object</returns>
    public GameObject GameObjectPool()
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy)
            {
                pooledObjects[i].SetActive(true);
                pooledObjects[i].GetComponent<SmallCircleScript>().Refresh();
                return pooledObjects[i];
            }
        }

        GameObject obj = (GameObject) Instantiate(poolObject);
        obj.GetComponent<SmallCircleScript>().Refresh();
        obj.transform.SetParent(this.gameObject.transform);
        pooledObjects.Add(obj);
        obj.SetActive(true);
        return obj;
    }
}
