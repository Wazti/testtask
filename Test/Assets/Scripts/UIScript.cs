﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIScript : MonoBehaviour
{
    private MainScript _mainScript;

    private TextMeshProUGUI _score;
   
    private TextMeshProUGUI _timer;
    public bool TimerEnd;
    // Start is called before the first frame update
    void Start()
    {
        _mainScript = GameObject.FindWithTag("MainCamera").GetComponent<MainScript>();
        _score = gameObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        _timer = gameObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        gameObject.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Game Over";
        gameObject.transform.GetChild(3).GetChild(0).GetComponent<TextMeshProUGUI>().text = "Restart";
        TimerEnd = true;
        
        //Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }

    // Update is called once per frame
    void Update()
    {
        _score.text = "Score : " + _mainScript.Score;
    }

    /// <summary>
    /// Coroutine, that set a timer and display on UI
    /// </summary>
    /// <returns></returns>
    public IEnumerator Timer() {
        this.gameObject.transform.GetChild(1).gameObject.SetActive(true);
        for (int f = 4; f >= 1; --f)
        {
            _timer.text = f.ToString();
            yield return new WaitForSeconds(0.7f);
        }
        this.gameObject.transform.GetChild(1).gameObject.SetActive(false);
        TimerEnd = true;
        _mainScript.Status = "round";
    }
    public void End()
    {
        gameObject.transform.GetChild(2).gameObject.SetActive(true);
        gameObject.transform.GetChild(3).gameObject.SetActive(true);
    }
}
