﻿
using UnityEngine;
using System.Collections;

public class CameraShakeScript : MonoBehaviour
{
  
	
    public float shakeDuration;
	
    // Amplitude of the shake. A larger value shakes the camera harder.
    public float shakeAmount;
    public float decreaseFactor; 
	
    Vector3 originalPos;

    private void Start()
    {
        shakeDuration = 0f;
        shakeAmount = 0.1f;
        decreaseFactor = 1.0f;
    }
    

    void OnEnable()
    {
        originalPos = transform.localPosition;
    }

    void Update()
    {
        
        if (shakeDuration > 0) // if duration > 0 , shake the camera
        {
            transform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;
			
            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = 0f;
            transform.localPosition = originalPos;
        }
    }
}