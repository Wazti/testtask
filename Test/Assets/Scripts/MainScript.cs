﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainScript : MonoBehaviour
{
    private Vector3 _mousPos;

    public Vector3 MousePos
    {
        get { return _mousPos; }
        set
        {
            var posx = Camera.main.transform.position.x;
            var posy = Camera.main.transform.position.y;
            if (value.x >= posx - widthCamera / 2 && value.x <= posx + widthCamera / 2 &&
                value.y >= posy - heightCamera / 2 &&
                value.y <= posy + heightCamera / 2)
            {
                _mousPos = value;
            }
            else
            {
                if (Status == "round")
                {
                    Status = "end";
                }
            }
        }
    }

    private bool _roundGoing;
    public int Score;
    public int Round;
    public int CirclesPerRound;
    public int LeftCircles;
    public float widthCamera;
    public float heightCamera;
    private float _slowdownFactor = 0.05f;
    private float _slowdownLength = 2f;
    public string Status;

    private ObjectPooling _circlePooling;
    private UIScript _ui;

    // Start is called before the first frame update
    void Start()
    {
        Status = "start";
        Score = 0;
        Round = 1;
        CirclesPerRound = 10;
        MousePos = new Vector3(0, 0, 0);
        _ui = GameObject.FindWithTag("Canvas").GetComponent<UIScript>();
        _circlePooling = GameObject.FindWithTag("PoolingCircle").GetComponent<ObjectPooling>();
        Camera cam = Camera.main;
        heightCamera = 2f * cam.orthographicSize;
        widthCamera = heightCamera * cam.aspect;
    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale += (1f / _slowdownLength) * Time.unscaledDeltaTime;
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
        CheckStatus();
        MousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    /// <summary>
    /// Function, that check a current state of game
    /// </summary>
    void CheckStatus()
    {
        switch (Status)
        {
            case "start":
            {
                Status = "timer";
                break;
            }
            case "timer":
            {
                if (_ui.TimerEnd)
                {
                    GenerateCircles(Round * CirclesPerRound);
                    LeftCircles = Round * CirclesPerRound;
                    StartCoroutine(_ui.Timer());
                    _ui.TimerEnd = false;
                }

                break;
            }
            case "round":
            {
                if (LeftCircles == 0)
                {
                    Round++;
                    Status = "timer";
                }

                break;
            }
            case "end":
            {
                _ui.End();
                StartCoroutine(KillEveryThing());
                Status = "restart";
                break;
            }
        }
    }

    public void DoSlowmotion()
    {
        Time.timeScale = _slowdownFactor;
        Time.fixedDeltaTime = Time.timeScale * .02f;
    }

    public void Shake()
    {
        gameObject.GetComponent<CameraShakeScript>().shakeDuration = 0.5f;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Function, that genereate circles with curtain amount
    /// </summary> an amount of circles 
    /// <param name="amount"></param>
    private void GenerateCircles(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            _circlePooling.GameObjectPool();
        }
    }

    /// <summary>
    /// Coroutine, that destroy all circles in game
    /// </summary>
    /// <returns></returns>
    public IEnumerator KillEveryThing()
    {
        for (int f = 0; f < _circlePooling.pooledObjects.Count; ++f)
        {
            if (_circlePooling.pooledObjects[f].activeInHierarchy)
            {
                DoSlowmotion();
                _circlePooling.pooledObjects[f].GetComponent<SmallCircleScript>().Death();
                yield return new WaitForSeconds(0.3f);
            }
        }
    }
}