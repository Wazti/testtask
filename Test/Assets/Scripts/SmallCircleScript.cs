﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

[ExecuteInEditMode]
public class SmallCircleScript : MonoBehaviour
{
    [SerializeField] [Range(0, 0.5f)] private float _size;

    public float Size
    {
        get { return _size; }
        set
        {
            this.gameObject.transform.localScale = new Vector3(value, value, value);
            _size = value;
            gameObject.GetComponent<TrailRenderer>().startWidth = _size * 20;
            _speed = _ratioSpeedSize / _size;
        }
    }

    [SerializeField] private float _speed;

    public float Speed
    {
        get { return _speed; }
        set
        {
            _speed = value;
            _size = _ratioSpeedSize / _speed;
        }
    }

    [SerializeField] 
    private float _ratioSpeedSize;
    [SerializeField] 
    protected Sprite texture;
    private SpriteRenderer _color;
    private MainScript _mainScript;

    public ParticleSystem Particle;

    // Start is called before the first frame update
    void Start()
    {
        _mainScript = Camera.main.gameObject.GetComponent<MainScript>();
        Gradient gradient = new Gradient();
        gradient.SetKeys(
            new GradientColorKey[]
            {
                new GradientColorKey(gameObject.GetComponent<SpriteRenderer>().color, 0.0f),
                new GradientColorKey(Color.white, 1.0f)
            },
            new GradientAlphaKey[] {new GradientAlphaKey(1, 0.0f), new GradientAlphaKey(0, 1.0f)});
        gameObject.GetComponent<TrailRenderer>().colorGradient = gradient;
        _color = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Size = _size;
        Speed = _speed;
    }

    private void FixedUpdate()
    {
        if (_mainScript.Status == "round")
        {
            gameObject.transform.position = Vector2.MoveTowards(gameObject.transform.position, _mainScript.MousePos,
                Speed * Time.deltaTime);
        }
    }

    public void Death()
    {
        if (_mainScript.Status == "round")
        {
            _mainScript.Score++;
            _mainScript.LeftCircles--;
        }

        _mainScript.Shake();
        ParticleSystem _particle = (ParticleSystem) Instantiate(Particle);
        _particle.transform.position = transform.position;
        _particle.Play();
        gameObject.GetComponent<TrailRenderer>().Clear();
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer << 9 != 0 && _mainScript.Status == "round")
        {
            Death();
        }
    }

    public void Refresh()
    {
        this.Start();
        Size = Random.Range(0.015f, 0.025f);
        transform.position = RandomPosition();
    }

    private void OnMouseEnter()
    {
        _mainScript.Status = "end";
        _mainScript.DoSlowmotion();
    }

    private Vector2 RandomPosition()
    {
        var side = Random.Range(0, 4);
        var posy = Camera.main.transform.position.y;
        var posx = Camera.main.transform.position.x;
        switch (side)
        {
            case 0:
            {
                return new Vector2(Random.Range(posx - _mainScript.widthCamera / 2, posx + _mainScript.widthCamera / 2),
                    posy + _mainScript.heightCamera / 2 + 2f);
            }
            case 2:
            {
                return new Vector2(Random.Range(posx - _mainScript.widthCamera / 2, posx + _mainScript.widthCamera / 2),
                    posy - _mainScript.heightCamera / 2 - 2f);
            }
            case 1:
            {
                return new Vector2(posx + _mainScript.widthCamera / 2 + 2f,
                    Random.Range(posy - _mainScript.heightCamera / 2, posy + _mainScript.heightCamera / 2));
            }
            case 3:
            {
                return new Vector2(posx - _mainScript.widthCamera / 2 - 2f,
                    Random.Range(posy - _mainScript.heightCamera / 2, posy + _mainScript.heightCamera / 2));
            }
            default:
            {
                return Vector2.one;
            }
        }
    }
}