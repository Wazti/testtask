﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareScript : MonoBehaviour
{
    private MainScript _mainScript;

    // Start is called before the first frame update
    void Start()
    {
        _mainScript = GameObject.FindWithTag("MainCamera").GetComponent<MainScript>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    /// <summary>
    /// Detects mouse on collider of square
    /// </summary>
    private void OnMouseEnter()
    {
        if (_mainScript.Status == "round")
        {
            _mainScript.DoSlowmotion();
            _mainScript.Status = "end";
        }
    }
}